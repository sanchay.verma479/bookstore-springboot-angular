package com.book.store.bookstore.Controller;

import com.book.store.bookstore.Dto.BookDto;
import com.book.store.bookstore.Entity.Book;
import com.book.store.bookstore.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.POST, value="/book")
    public String addBook(@RequestBody BookDto bookDto){
        Book book = new Book(bookDto.getName(),bookDto.getAuthor(),bookDto.getPublication(),
                bookDto.getAvailableQuantity(),bookDto.getPrice());
        return bookService.addBook(book);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/book")
    public List<Book> getAllBook(){
        return bookService.getAllBook();
    }

    @RequestMapping(method= RequestMethod.GET, value = "/book/{id}")
    public Optional<Book> getBookById(@PathVariable("id")int id){
        return bookService.getBookById(id);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/book")
    public String updateBook(@RequestBody BookDto bookDto){
        Book book = new Book(bookDto.getId(),bookDto.getName(),bookDto.getAuthor(),bookDto.getPublication(),
                bookDto.getAvailableQuantity(),bookDto.getPrice());
        return bookService.updateBook(book);
    }
    @RequestMapping(method = RequestMethod.DELETE , value = "/book/{id}")
    public String deleteBook(@PathVariable("id")int id){
        return bookService.deleteBook(id);
    }
}
